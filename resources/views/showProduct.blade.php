@extends('layouts.structure')
@section('title', 'Songja-dev')

@section('content')
    @if($price >= 100)
        <p>รองเท้า</p>
    @elseif($price >= 70 && $price <= 99)
        <p>เสื้อผ้า</p>
    @else
        <p>หนังสือเรียน</p>
    @endif

    @for($i = 1; $i <= 10; $i++)
        <br><a href="#">Page {{$i}}</a>
    @endfor

    @foreach($users as $user)
        <h2>{{$user}}</h2>
    @endforeach
@endsection


