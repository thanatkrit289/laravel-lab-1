<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
</head>
<body>
    <div class="header">
        <ul>
            <li>Home</li>
            <li>Login</li>
            <li>Register</li>
        </ul>
    </div>

    <div class="content">
        @yield('content')
    </div>

    <div class="footer">
        <p>CopyRight : Songja-dev</p>
    </div>
</body>
</html>

