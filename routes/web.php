<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', 'ProductController@contact');

//Route::view('/show', 'menu');
//Route::view('/contact/email', 'menu')->name('cmail');
//Route::view('/contact/tel', 'menu')->name('ctel');

Route::get('/blog', 'ProductController@blog');

Route::get('/products', 'ProductController@index');
