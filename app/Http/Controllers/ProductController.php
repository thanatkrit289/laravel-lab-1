<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(){
        $price = 89;
        $user = ['A', 'B', 'C'];
//    return view('showProduct')->with('price', $price);
        return view('showProduct', ['users' => $user, 'price' => $price]);
    }

    public function contact(){
        $price = 5000000;
        $description = '11111111111111111112222222222222223333333333333333344444444444445555555555555566666666666677777777777';

        return view('contact')
            ->with('price', $price)
            ->with('description', $description);
    }

    public function blog(){
        return view('blog');
    }
}
